(async () => {

    const
        { writeFileSync } = require('fs'),
        trigedasleng = require('..');

    const
        missingTypes = [],
        missingTranslations = [],
        missingEtymology = [],
        typesStats = {};

    const dictionary = await trigedasleng.getDictionary();

    for(let i = 0; i < dictionary.length; i++){

        const { text, types, translations, etymology } = dictionary[i];

        if(!types.length || types.includes('none')) missingTypes.push(text);

        if(!translations.length) missingTranslations.push(text);

        if(!etymology) missingEtymology.push(text);

        for(let j = 0; j < types.length; j++){

            const type = types[j];

            if(!typesStats[type]) typesStats[type] = 1;

            else typesStats[type]++;

        }

    }

    let report = `# Trigedasleng database test report ${new Date().toISOString()}`;

    if(missingTypes.length || missingTranslations.length || missingEtymology.length) report += `\n\n## Issues`;
    if(missingTypes.length) report += `\n\n### Missing types (${missingTypes.length})\n\n\`\`\`\n${missingTypes.join('\n')}\n\`\`\``;
    if(missingTranslations.length) report += `\n\n### Missing translations (${missingTranslations.length})\n\n\`\`\`\n${missingTranslations.join('\n')}\n\`\`\``;
    if(missingEtymology.length) report += `\n\n### Missing etymology (${missingEtymology.length})\n\n\`\`\`\n${missingEtymology.join('\n')}\n\`\`\``;

    report += `\n\n## Stats`;
    report += `\n\n### Types (${Object.keys(typesStats).length})\n\nType | Count\n--- | ---\n${Object.entries(typesStats).sort((a, b) => b[1] - a[1]).map(item => `${item[0]} | ${item[1]}`).join('\n')}`;

    writeFileSync('README.md', report, 'utf8');

})();