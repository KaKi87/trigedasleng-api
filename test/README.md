# Trigedasleng database test report 2020-05-30T17:50:27.363Z

## Issues

### Missing etymology (479)

```
aifou
as raun
ashen
asken
audahir
aulana
azklodon
azmellen
bastab in
bastaba
bastabfou
bastabplei
bela
belen
belen bouz
belplei
ben
ben raun
benfou
bernes
bet
bezer
bigas
binch
bisfou
bisgeda
blek
blen op
blid thru
bliden
bling
blou au
blou we
blounen
blouplei
blouwen
bol au
bolen
boren
bouwi
bozi
bozines
breik daun
breik in
breik klin
breik of
breik op
breik thru
breik we
breina
brolhaken
buken
bumstepa
bushchoja
bushchojen
bushfou
bushon
but au
but op
byoufou
byouwen
chants op
chich raun
chints of
chit
chof
chofnes
chopro
chouka
daunon
dednes
dig in
dofo
drag au
drag we
drein we
drop daun
drop klin
dufaplei
dula kainnes
dula klin
emon
enthing
ewon
eyon
fang
fanggi
fayagon pauda
feis-staba
feisbatel op
fiden
firnes
fisen
fisenon
fiskidon
fiyon
flagen
flagon
flapa
flapkrasha
flasha
fleil raun
flinga
fluf op
flufi
flufines
flufkova
fok op
foka
fokfou
fokiten
fokop
fong raun
fossopen
foto
fou gonon
fousad
fousen
fousfou
fousnes
frag klin
frouzen
fyum op
fyumen
gada
gada raun
gaujen
ge skrud op
gei op
geifou
geines
get thru
getnes
geyon
ginteiknes
glomp op
glong klin
glong we
glop
gochu op
gon bum
gontaim
gonz
gor thru
goren
goufas
gougenas
gougou
gozila
grab op
graben
gran thru
gredi-bloka
gredines
grediyas
groun au
grouna
grounen
grous
gyon
haden
hadgeda
hadnes
haisidon op
haka
haken-de
hakplei
han au
han daun
han op (item)
hanga
hefnes
hepa
heri sich
hisa
hit of
hiton
hob
hob raun
hod au
honen
hosh klin
hosta we
houd of
houden
houdfou
housom
huk in
ida
idowe
inayu
jimi
jomp au
jostaim
jova
kainfou
kampa
keijen
keim thru
kep au
kep op
kep we
kik au
kiknes
klam daun
klama
klamen
klamon
kok au
kola
kom nau
kom thru
koma
koma op
komfonen
kot au
kot klin
koven
kozplei op
kozpleya
krouka
lagfou
laiffou
laksen
lan op
lanfou
langeden
lannes
lat klin
lat we
laten
latfou
lein au
let daun
let op
let thru
lev op
lid of
lim
limhanga
linkon
lol
lol au
lol klin
lol op
lolen
lolfou
lolnes
lolplei
longtaim
lou
louded
lukotnes
mana klin
mash op
maya op
mel of
mel op
mellen
mema daun
mema op
mida
mouj raun
moujen
moustaim
naifstik
natflaya
natsoujon
nattaim
nautaim
nida
ninja ... raun
nod we
nogredines
nosho
nou longa
nou stepnes
noyus
obz
oda
odi
ofon
om
omi
omo
omon
os job
oseim
oudas
ounnes
pad
pak au
pak op
paken
pakkru
pakon
pauda
paudafou
plannes
plei raun
pleni os
poda
podiyen
poko daun
pokowen
pol au
pol of
pomi
pot of
pot op
praimfaya
praya
pro
promada
radzfou
radzhaknes
radznes
ragen
ragjus
ragtaim
rai
raiza
raizen
raswap
reijen
reinbloka
reinen
rekfou
reknes
reya
reya
rornes
rowa
rutas
sain
sainen
saut
seifas
sen in
sen klin
sennes
sesori
shan thru
shannen
sheidklodon
shiles
shilfou
shilnes
shofplei
shoga
shouda
shoun op
shov of
shuda-haka
shuk
sin klin
sin thru
sirisfou
skafa
skaiflufen
skat
skin
skin daun
skinen
skrach op
skracha
skrap
skraunj op
skrudon
skruwen
skruwon
skwik au
skwikfou
skwiknes
skwila
skyumi
slad au
slak
slip we
smak op
smol
smouka
smouken
snapnes
sofkova
sofstepa
softwicha
somi
somines
somiyon
somon
somtaim
son-geda
sonchafou
sonfou
songon
soren
soriyas
souda
souk thru
souken
souknes
spil
spil au
splatten
stabfou
staggeda
stagmaplei
stashen
stashnes
stashon
steda
step daun
stira
stok daun
stomba of
stot op
strechon
strik op
sukola
sunas
swapa
swega we
swela
swich au
swich of
swish au
swisha
swoulon
tagen
taingeda
talia
talifaya
taliyon
teina
teis op
teisen
teisi
teksora
thing
throu au
throu of
thruwen
tikfou
tiknes
toch op
tol
ton op
top
trap
trau
tri-wilou
tridropa
trijus
triklama
trishuga
tro
trompa
truthen
tush
veidon
voudon
vout
wan raun
wedafou
wich in
wich op
wichfou
wichnes
woda trei
wonkru
woukfou
yomblod
yon
yon
yon op
yongnes
yonhaka
yunikon
```

## Stats

### Types (15)

Type | Count
--- | ---
noun | 884
verb | 498
adjective | 345
adverb | 70
phrase | 49
pronoun | 32
proper noun | 27
number | 17
preposition | 14
interjection | 12
auxiliary | 9
conjunction | 7
satellite | 4
idiom | 4
particle | 1