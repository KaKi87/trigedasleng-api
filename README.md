# trigedasleng-api

NodeJS API wrapper for [trigedasleng.net](https://trigedasleng.net/) by Stephano from Project Arkadia.

## Getting started

### Prerequisites

- NodeJS
- NPM
- Yarn

### Install

From [npm](https://www.npmjs.com/package/trigedasleng-api)

`yarn add trigedasleng-api`

or

`npm i trigedasleng-api`

## Use

```js
const trig = require('trigedasleng-api');
```

### Search

```js
trig.search(query, lang)
	.then(console.log)
	.catch(console.error);
```

Parameters :
- `query` *string* - A word/expression to translate
- `lang` *string* - Query language, either `eng` or `trig`
<br>*(Optional but enhances results sorting)*

Result :
- `exactMatch` *array* - Exact matching words
- `words` *array* - Other matching words sorted by relevancy
    - Cf. word/phrase object structure
    - `match` *float*
- `sentences` *array* - Translated sentences from the show
	- `trig` *string* - Sentence in trigedasleng
	- `eng` *string* - Sentence in english
	- `leipzig` *string* - Sentence in Leipzig Glossing notation
	- `etymology` *string* - The word's origin
	- `episode` - The episode which the sentence come from
		- `season` *integer*
		- `number` *integer*
	- `audio` *url* - The sentence recording from the episode
	- `match` *float*

### Dictionnary

```js
trig.getDictionary(dic)
	.then(console.log)
	.catch(console.error);
```

`dic` *string* - Dictionnary type
<br>Default : `all`

Known :
	- `canon`
	- `noncanon`
	- `slakgedasleng`

Result : an array containing all words from all dictionaries.

### Word/phrase object structure

- `link` *url* - Link to the word's page from trigedasleng.net
- `text` *string* - The word/phrase itself
- `type` *array* of *strings* - The word/phrase type(s)
<br>Most known :
	- `noun`
	- `verb`
	- `adjective`
	- `adverb`
	- `phrase`
	- `interjection`
	- `preposition`
	- `auxiliary`
	- `conjunction`
	- `satellite`
	- `idiom`
- `translations` *array* of *objects* - Matching translations for the word/phrase
	- `before` *string* - (usually) specific word type
	- `text` *string* - word
	- `after` *string* - (usually) specific word context
	- `fullText` *string* - the entire translation string
- `etymology` *string*

### Word/phrase types

```js
trig.getTypes()
	.then(console.log)
	.catch(console.error);
```

Result : associative array
<br>Key : word/phrase type
<br>Value : word/phrase count

### Translation

```js
trig.translate(sentence, lang)
	.then(console.log)
	.catch(console.error);
```

Result : literal (word-by-word) translated string

Untranslated words will remain in english.

## Built with

Node modules :
- [requestretry](https://github.com/FGRibreau/node-request-retry) - HTTP requests with auto-retry (based on [request](https://github.com/request/request))
<br>To make requests to API endpoints
- [string-similarity](https://github.com/aceakash/string-similarity) - Dice's coefficient based strings similarity calculator
<br>To enhance results sorting

## Credits

- Jensen (Lead developer of Trigedasleng.info) [Tumblr](http://smallerontheoutside.tumblr.com/) 
- David J. Peterson (language creator) [@dedalvs](http://twitter.com/dedalvs)
- The CW (show rights) [@cwthe100](https://twitter.com/cwthe100)
- Sloan (Developer for Trigedasleng.info)
- Stephano (Developer of [Trigedasleng.net](https://trigedasleng.net/) and creator of Project Arkadia — [Discord](https://discord.gg/Gx7hzZg), [Twitter](https://twitter.com/Project_Arkadia), [Forum](https://projectarkadia.com/))

## Changelog

- `1.0.0` (2019-06-03) • Initial release
- `1.1.0` (2019-07-26)
	- Improved search results parsing
    - Added support for dictionary filters to `getDictionary` method
    - Added `getTypes` & `translate` method
- `1.1.2` (2020-05-18)
    - Fixed typeless words
    - Using Laravel API endpoints