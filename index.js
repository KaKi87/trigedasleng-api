const
	request = require('requestretry'),
	stringSimilarity = require('string-similarity'),
	PQueue = require('p-queue').default;

const host = 'https://trigedasleng.net';

const parseWordItem = item => {
	const
		translation = [],
		etymology = item['etymology'] ? item['etymology'].replace(/^from:? /, '').replace(/"/g, '') : null,
		word = item['word'].toLowerCase();
	if(item['translation']){
		const
			t = item['translation'],
			c = t.indexOf(':') !== -1;
		translation.push(c ? t.split(':')[0].trim() : null);
		translation.push(c ? t.split(':')[1].trim() : t);
	}
	return {
		link: encodeURI(`${host}/word/${word}`),
		text: word,
		...(translation.length > 0 ? {
			types: translation[0] ? translation[0].split('/') : [],
			translations: translation[1].split(/[;,]/).map(item => item.toLowerCase().trim().match(/^(\((.+)\) )?(.+?)( \((.+)\))?$/)).map(item => ({
				...(item[2] ? { before: item[2] } : {}),
				text: item[3],
				...(item[5] ? { after: item[5] } : {}),
				fullText: `${item[2] ? `(${item[2]}) ` : ''}${item[3]}${item[5] ? ` (${item[5]})` : ''}`
			})).filter(t => !!t)
		} : {}),
		...(etymology && etymology !== 'unknown' ? { etymology } : {})
	}
};

const get = (action, args) => new Promise((resolve, reject) => request(`${host}/api/legacy/${action}`, {
	json: true,
	qs: { ...args }
}, (err, res, body) => {
	if(err) return reject('CLIENT_ERROR');
	if(body.length && body.length === 0) return reject(typeof body === 'string' ? 'EMPTY_BODY' : 'NO_RESULT');
	switch(parseInt(res.statusCode.toString()[0])){
		case 2:
			return resolve(body);
		case 4:
			if(res.statusCode === 420) return reject('RATE_LIMIT_REACHED');
			return reject('CLIENT_ERROR');
		case 5:
			return reject('SERVER_ERROR');
	}
}));

module.exports = {
	search: (query, lang) => new Promise((resolve, reject) => {
		if(lang && ['trig', 'eng'].indexOf(lang) === -1) return reject('INVALID_LANG');
		get('search', { query }).then(res => {
			let words, exactMatch;
			if(res['words']){
				words = res['words'].map(item => parseWordItem(item));
				for(let i = 0; i < words.length; i++){
					switch(lang){
						case 'trig':
							words[i].match = stringSimilarity.compareTwoStrings(query, words[i].text);
							break;
						case 'eng':
							words[i].match = Math.max(...words[i].translations.map(translation => stringSimilarity.compareTwoStrings(query, translation.text)));
							break;
					}
				}
				exactMatch = words.filter(word => word.match === 1);
			}
			resolve({
				...(exactMatch ? { exactMatch } : {}),
				...(words ? { words: words.filter(word => word.match < 1 || !word.match).sort((a, b) => b.match - a.match) } : {}),
				...(res['translations'] ? {
					sentences: res['translations'].map(item => {
						const text = {
							trig: item['trigedasleng'],
							eng: item['translation']
						};
						return {
							trig: text.trig,
							eng: text.eng,
							...(item['leipzig'] ? { leipzig: item['leipzig'] } : {}),
							...(item['etymology'] ? { etymology: item['etymology'] } : {}),
							...((item['episode'] && item['episode'] !== 'other') ? {
								episode: {
									season: parseInt(item['episode'].slice(0, 2)),
									number: parseInt(item['episode'].slice(2, 4))
								}
							} : {}),
							...(item['audio'] ? { audio: `${host}/${item['audio']}` } : {}),
							...(lang ? {
								match: stringSimilarity.compareTwoStrings(query, text[lang])
							} : {})
						}
					}).sort((a, b) => b.match - a.match)
				} : {}),
				link: encodeURI(`${host}/search?q=${query}`)
			});
		}).catch(reject);
	}),
	getDictionary: dic => new Promise((resolve, reject) => {
		get('dictionary', { filter: dic || 'all' })
			.then(res => resolve(res.map(word => parseWordItem(word)).sort((a, b) => a.text.localeCompare(b.text))))
			.catch(reject);
	}),
	getTypes: () => new Promise((resolve, reject) => {
		module.exports.getDictionary()
			.then(dic => {
				const types = {};
				dic.forEach(item => {
					item.types.forEach(type => {
						if(types[type] === undefined)
							types[type] = 1;
						else
							types[type]++;
					});
				});
				resolve(types);
			})
			.catch(reject);
	}),
	translate: (sentence, lang) => new Promise((resolve, reject) => {
		let translation = '';
		new PQueue({ concurrency: 1 }).addAll(sentence.split(/\s/).map(word => () => new Promise(resolve => {
			module.exports.search(word.toLowerCase(), lang).then(res => {
				translation += res.exactMatch ? (lang === 'eng' ? res.exactMatch[0].text : res.exactMatch[0].translations[0].text) : word;
				resolve();
			}).catch(() => {
				translation += word;
				resolve();
			}).finally(() => {
				translation += ' ';
			})
		}))).then(() => {
			resolve(translation);
		}).catch(reject);
	})
};